﻿using Brunoprojeto.DbCodigos.Pedido;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Brunoprojeto.Telas
{
    public partial class Consultar_pedido : Form
    {
        public Consultar_pedido()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PedidoBusiness business = new PedidoBusiness();
            List<Pedidocliente> lista = business.Consultar(txtnome.Text);

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = lista;

        }

        private void adicionarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_de_produto tela = new Cadastro_de_produto();
            tela.Show();

        }

        private void consultarProdutoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Consultar_produto tela = new Consultar_produto();
            tela.Show();
        }

        private void adicionarPedidoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Cadastro_de_pedido tela = new Cadastro_de_pedido();
            tela.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Tela_de_inicio tela = new Tela_de_inicio();
            tela.Show();
            this.Close();
        }
    }
}

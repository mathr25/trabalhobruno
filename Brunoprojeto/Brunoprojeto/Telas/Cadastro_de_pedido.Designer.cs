﻿namespace Brunoprojeto.Telas
{
    partial class Cadastro_de_pedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cboprodutos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtqtn = new System.Windows.Forms.TextBox();
            this.dgvitens = new System.Windows.Forms.DataGridView();
            this.Produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnsalvar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produtoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.adicionarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.adicionarPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.cbopgto = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvitens)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(37, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(306, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Escolha seu produto";
            // 
            // cboprodutos
            // 
            this.cboprodutos.FormattingEnabled = true;
            this.cboprodutos.Location = new System.Drawing.Point(43, 77);
            this.cboprodutos.Name = "cboprodutos";
            this.cboprodutos.Size = new System.Drawing.Size(300, 21);
            this.cboprodutos.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(37, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 33);
            this.label2.TabIndex = 2;
            this.label2.Text = "Quantidade";
            // 
            // txtqtn
            // 
            this.txtqtn.Location = new System.Drawing.Point(43, 153);
            this.txtqtn.Name = "txtqtn";
            this.txtqtn.Size = new System.Drawing.Size(300, 20);
            this.txtqtn.TabIndex = 3;
            // 
            // dgvitens
            // 
            this.dgvitens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvitens.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Produto});
            this.dgvitens.Location = new System.Drawing.Point(452, 50);
            this.dgvitens.Name = "dgvitens";
            this.dgvitens.Size = new System.Drawing.Size(202, 221);
            this.dgvitens.TabIndex = 4;
            // 
            // Produto
            // 
            this.Produto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Produto.DataPropertyName = "nome";
            this.Produto.HeaderText = "Produto";
            this.Produto.Name = "Produto";
            // 
            // btnsalvar
            // 
            this.btnsalvar.Font = new System.Drawing.Font("Georgia", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalvar.Location = new System.Drawing.Point(43, 302);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(611, 68);
            this.btnsalvar.TabIndex = 5;
            this.btnsalvar.Text = "Adicionar Pedido";
            this.btnsalvar.UseVisualStyleBackColor = true;
            this.btnsalvar.Click += new System.EventHandler(this.btnsalvar_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produtoToolStripMenuItem1,
            this.pedidoToolStripMenuItem1});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // produtoToolStripMenuItem1
            // 
            this.produtoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adicionarProdutoToolStripMenuItem,
            this.consultarProdutoToolStripMenuItem});
            this.produtoToolStripMenuItem1.Name = "produtoToolStripMenuItem1";
            this.produtoToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.produtoToolStripMenuItem1.Text = "Produto";
            // 
            // adicionarProdutoToolStripMenuItem
            // 
            this.adicionarProdutoToolStripMenuItem.Name = "adicionarProdutoToolStripMenuItem";
            this.adicionarProdutoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.adicionarProdutoToolStripMenuItem.Text = "Adicionar produto";
            // 
            // consultarProdutoToolStripMenuItem
            // 
            this.consultarProdutoToolStripMenuItem.Name = "consultarProdutoToolStripMenuItem";
            this.consultarProdutoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.consultarProdutoToolStripMenuItem.Text = "consultar produto";
            // 
            // pedidoToolStripMenuItem1
            // 
            this.pedidoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adicionarPedidoToolStripMenuItem,
            this.consultarPedidoToolStripMenuItem});
            this.pedidoToolStripMenuItem1.Name = "pedidoToolStripMenuItem1";
            this.pedidoToolStripMenuItem1.Size = new System.Drawing.Size(117, 22);
            this.pedidoToolStripMenuItem1.Text = "Pedido";
            // 
            // adicionarPedidoToolStripMenuItem
            // 
            this.adicionarPedidoToolStripMenuItem.Name = "adicionarPedidoToolStripMenuItem";
            this.adicionarPedidoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.adicionarPedidoToolStripMenuItem.Text = "Adicionar Pedido";
            // 
            // consultarPedidoToolStripMenuItem
            // 
            this.consultarPedidoToolStripMenuItem.Name = "consultarPedidoToolStripMenuItem";
            this.consultarPedidoToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.consultarPedidoToolStripMenuItem.Text = "Consultar Pedido";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(185, 197);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Adicionar ao Carrinho";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Forma de pagamento";
            // 
            // cbopgto
            // 
            this.cbopgto.FormattingEnabled = true;
            this.cbopgto.Items.AddRange(new object[] {
            "Dinheiro",
            "Cartão de debito",
            "Cartão de credito"});
            this.cbopgto.Location = new System.Drawing.Point(43, 197);
            this.cbopgto.Name = "cbopgto";
            this.cbopgto.Size = new System.Drawing.Size(121, 21);
            this.cbopgto.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(539, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Voltar";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // Cadastro_de_pedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(684, 382);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbopgto);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.dgvitens);
            this.Controls.Add(this.txtqtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboprodutos);
            this.Controls.Add(this.label1);
            this.Name = "Cadastro_de_pedido";
            this.Text = "Cadastro_de_pedido";
            ((System.ComponentModel.ISupportInitialize)(this.dgvitens)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboprodutos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtqtn;
        private System.Windows.Forms.DataGridView dgvitens;
        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem adicionarProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarProdutoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem adicionarPedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarPedidoToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Produto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbopgto;
        private System.Windows.Forms.Label label4;
    }
}
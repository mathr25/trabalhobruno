﻿using Brunoprojeto.DbCodigos.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Brunoprojeto.Telas
{
    public partial class Cadastro_de_produto : Form
    {
        public Cadastro_de_produto()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void valor_Click(object sender, EventArgs e)
        {

        }

        private void Cadastro_de_produto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (sender == null)
            {
                throw new ArgumentNullException(nameof(sender));
            }

            ProdutoDTO dto = new ProdutoDTO();
            dto.nome = txtproduto.Text;
            dto.decricao = txtdescricao.Text;
            dto.valor = Convert.ToDecimal(txtvalor.Text);

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Produto salvo com sucesso.");
            this.Hide();

        }

        private void label2_Click(object sender, EventArgs e)
        {
            Tela_de_inicio tela = new Tela_de_inicio();
            tela.Show();
            this.Close();
        }
    }
}

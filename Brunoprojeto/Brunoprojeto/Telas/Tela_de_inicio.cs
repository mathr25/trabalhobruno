﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Brunoprojeto.Telas
{
    public partial class Tela_de_inicio : Form
    {
        public Tela_de_inicio()
        {
            InitializeComponent();
        }
        private void consultarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultar_produto tela = new Consultar_produto();
            tela.Show();
            this.Hide();
        }

        private void adicionarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
          Telas.Cadastro_de_pedido tela = new Telas.Cadastro_de_pedido();
            tela.Show(tela);
        }

        private void consultarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Consultar_pedido tela = new Consultar_pedido();
            tela.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Consultar_produto tela = new Consultar_produto();
            tela.Show();
            Tela_de_inicio tela1 = new Tela_de_inicio();
            tela1.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Cadastro_de_pedido tela = new Cadastro_de_pedido();
            tela.Show();
            Tela_de_inicio tela1 = new Tela_de_inicio();
            tela1.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Consultar_pedido tela = new Consultar_pedido();
            tela.Show();
            Tela_de_inicio tela1 = new Tela_de_inicio();
            tela1.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cadastro_de_produto tela = new Cadastro_de_produto();
            tela.Show();
            Tela_de_inicio tela1 = new Tela_de_inicio();
            tela1.Close();
        }

        private void adicionarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_de_produto tela = new Cadastro_de_produto();
            tela.Show();
          
        }

        private void consultarProdutoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Consultar_produto tela = new Consultar_produto();
            tela.Show();
        }

        private void adicionarPedidoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Cadastro_de_pedido tela = new Cadastro_de_pedido();
            tela.Show();
        }

        private void consultarPedidoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Consultar_pedido tela = new Consultar_pedido();
            tela.Show();
        }
    }
}

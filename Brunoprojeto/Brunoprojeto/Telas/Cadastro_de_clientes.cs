﻿using Brunoprojeto.DbCodigos.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Brunoprojeto.Telas
{
    public partial class Cadastro_de_clientes : Form
    {
        public Cadastro_de_clientes()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            dto.nome = txtnome.Text;
            dto.endereco = txtend.Text;
            dto.cep =txtcep.Text;
            dto.CPF = maskedTextBox1.Text;
            dto.login = txtlogin.Text;
            dto.senha = txtsenha.Text;

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Salvar(dto);

           
            MessageBox.Show("Cadastro realizado com sucesso");
            Form1 tela = new Form1();
            tela.Show();
        }

        private void txtcpf_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

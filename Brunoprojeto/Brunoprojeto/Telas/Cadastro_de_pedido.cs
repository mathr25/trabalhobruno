﻿using Brunoprojeto.DbCodigos;
using Brunoprojeto.DbCodigos.Funcionario;
using Brunoprojeto.DbCodigos.Pedido;
using Brunoprojeto.DbCodigos.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Brunoprojeto.Telas
{
    public partial class Cadastro_de_pedido : Form
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();

        public Cadastro_de_pedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }
        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboprodutos.ValueMember = nameof(ProdutoDTO.id);
            cboprodutos.DisplayMember = nameof(ProdutoDTO.nome);
            cboprodutos.DataSource = lista;
        }
        void ConfigurarGrid()
        {
            dgvitens.AutoGenerateColumns = false;
            dgvitens.DataSource = produtosCarrinho;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            
            dto.dt_de_entrega = DateTime.Now;

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.","Loja tudo",MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }


        private void adicionarProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_de_produto tela = new Cadastro_de_produto();
            tela.Show();

        }

        private void consultarProdutoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Consultar_produto tela = new Consultar_produto();
            tela.Show();
        }

        private void adicionarPedidoToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Cadastro_de_pedido tela = new Cadastro_de_pedido();
            tela.Show();
        }



        private void button1_Click_1(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboprodutos.SelectedItem as ProdutoDTO;


            int qtd = Convert.ToInt32(txtqtn.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void btnsalvar_Click(object sender, EventArgs e)
        {


            
            PedidoDTO dto = new PedidoDTO();
            dto.cliente = usersession.usuariologado.id;
            dto.dt_de_entrega = DateTime.Now;
            dto.forma_pgto = cbopgto.Text;
            

            PedidoBusiness business = new PedidoBusiness();
            business.Salvar(dto, produtosCarrinho.ToList());

            MessageBox.Show("Pedido salvo com sucesso.", "Loja tudo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            Tela_de_inicio tela = new Tela_de_inicio();
            tela.Show();
            this.Close();
        }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @" INSERT INTO tb_produto (nm_produto,ds_valor,ds_descricao) values (@nm_produto,@ds_valor,@ds_descricao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("ds_valor", dto.valor));
            parms.Add(new MySqlParameter("ds_descricao", dto.decricao));

            Database db = new Database();

            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE tb_produto
                                     SET nm_produto = @nm_produto,
                                          ds_valor   = @ds_valor,
                               WHERE id_produto = @id_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.id));
            parms.Add(new MySqlParameter("nm_produto", dto.nome));
            parms.Add(new MySqlParameter("ds_descricao", dto.decricao));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produto WHERE id_produto = @id_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<ProdutoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produto WHERE nm_produto like @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.id = reader.GetInt32("id_produto");
                dto.nome = reader.GetString("nm_produto");
                dto.valor = reader.GetDecimal("ds_valor");

                lista.Add(dto);
            }
            reader.Close();


            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.id = reader.GetInt32("id_produto");
                dto.nome = reader.GetString("nm_produto");
                dto.valor= reader.GetDecimal("ds_valor");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}

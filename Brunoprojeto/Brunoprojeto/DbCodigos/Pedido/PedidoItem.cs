﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Pedido
{
    class PedidoItem
    {
        public int  Id { get; set; }
        public int IdPedido { get; set; }
        public int IdProduto { get; set; }
    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Pedido
{
    class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (id_cliente,dt_de_entrega,forma_de_pgto) VALUES (@id_cliente,@dt_de_entrega,@forma_de_pgto)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.cliente));
            parms.Add(new MySqlParameter("dt_de_entrega", dto.dt_de_entrega));
            parms.Add(new MySqlParameter("forma_de_pgto", dto.forma_pgto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<Pedidocliente> Consultar (string cliente)
        {
            string script = @"SELECT * FROM vm_consultar WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Pedidocliente> lista = new List<Pedidocliente>();
            while (reader.Read())
            {
                Pedidocliente dto = new Pedidocliente();
              
                dto.Cliente = reader.GetString("nm_nome");
                dto.Produto = reader.GetString("nm_produto");
                dto.Valor = reader.GetInt32("ds_valor");
                dto.CPF = reader.GetString("ds_CPF");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Pedido
{
    class PedidoDTO
    {
        public int id { get; set; }
        public DateTime dt_de_entrega { get; set; }
        public string forma_pgto { get; set; }
        public int cliente { get; set; }

    }
}

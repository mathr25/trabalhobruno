﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Pedido
{
    class Pedidocliente
    {
        
        public string Cliente { get; set; }
        public int Valor { get; set; }
        public string Produto { get; set; }
        public string CPF { get; set; }
    }
}

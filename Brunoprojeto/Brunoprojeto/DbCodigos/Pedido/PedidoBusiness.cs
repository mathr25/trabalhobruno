﻿using Brunoprojeto.DbCodigos.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Pedido
{
    class PedidoBusiness
    {
       public int Salvar (PedidoDTO pedido, List<ProdutoDTO> produtos)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItem itemDTO = new PedidoItem();
                itemDTO.IdPedido = idPedido;
                itemDTO.IdProduto = item.id;

                itemBusiness.Salvar(itemDTO);
            }

            return idPedido;
        }

        public List<Pedidocliente> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }
    }
}

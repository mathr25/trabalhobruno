﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Funcionario
{
    class FuncionarioDTO
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string endereco { get; set; }
        public string cep { get; set; }
        public string CPF { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
    }
    
}
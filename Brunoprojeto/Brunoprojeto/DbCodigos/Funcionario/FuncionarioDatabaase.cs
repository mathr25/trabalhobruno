﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brunoprojeto.DbCodigos.Funcionario
{
    class FuncionarioDatabaase
    {
        public FuncionarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM tb_cliente WHERE ds_login = @ds_login AND ds_senha = @ds_senha";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", login));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO dto = null;
            if (reader.Read())
            {
                dto = new FuncionarioDTO();
                dto.id = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_nome");
                dto.endereco = reader.GetString("ds_endereco");
                dto.cep = reader.GetString("ds_cep");
                dto.CPF = reader.GetString("ds_CPF");
                dto.login = reader.GetString("ds_login");
                dto.senha = reader.GetString("ds_senha");
            }

            reader.Close();
            return dto;
        }
        public int Sslvar(FuncionarioDTO dto)
        {
            string script = @"INSERT tb_cliente(nm_nome,ds_endereco,ds_cep,ds_cpf,ds_login,ds_senha) 
                               VALUES (@nm_nome,@ds_endereco,@ds_cep,@ds_cpf,@ds_login,@ds_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.nome));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_login", dto.login));
            parms.Add(new MySqlParameter("ds_senha", dto.senha));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
